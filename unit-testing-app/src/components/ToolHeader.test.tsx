import * as React from 'react';
import { render, mount, shallow, ReactWrapper, ShallowWrapper, configure } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import * as Adapter from 'enzyme-adapter-react-16';

import { ToolHeader } from './ToolHeader';

configure({ adapter: new Adapter() });

describe('<ToolHeader /> Enzyme Static HTML', () => {

  test('<ToolHeader /> renders', () => {

    const component = JSON.stringify(render(
      <MemoryRouter>
        <ToolHeader headerText="The Tool" />
      </MemoryRouter>
    ).html());

    expect(component).toMatchSnapshot();

  });

});

describe('<ToolHeader /> Enzyme Mock DOM', () => {

  const headerTextValue = 'The Tool';
  let component: ReactWrapper;
  let h1Element: ReactWrapper;

  beforeEach(() => {

    component = mount(<MemoryRouter>
      <ToolHeader headerText={headerTextValue} />
    </MemoryRouter>);

    h1Element = component.find('h1');

  });

  test('<ToolHeader /> renders', () => {
    expect(h1Element.text()).toBe(headerTextValue);
  });

});

describe('<ToolHeader /> Shallow with Enzyme', () => {

  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<ToolHeader headerText="The Tool" />);
  });

  test('<ToolHeader /> renders', () => {

    // const h1Content = wrapper.props().children[0].props.children;
    expect(wrapper.find('h1').text()).toBe('The Tool');

  });

});