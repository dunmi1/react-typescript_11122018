import { Car } from '../models/Car';
import { CarActions, CarActionsUnion } from '../actions/car.actions';


export const carsReducer = (state: Car[] = [
  { id: 1, make: 'Tesla', model: 'Model S', year: 2018, color: 'red', price: 120000 },
  { id: 2, make: 'Nissan', model: 'Versa', year: 2014, color: 'blue', price: 14000 },
], action: CarActionsUnion) => {

  switch(action.type) {
    case CarActions.APPEND:
      const ids =  [...state.map(c => c.id)] as number[];
      const newCar = {
        ...action.payload,
        id: Math.max(...ids, 0) + 1,
      };
      return state.concat(newCar);
    case CarActions.REPLACE:
      const carIndex = state.findIndex(c => c.id === action.payload.id);
      const newCars = state.concat();
      newCars[carIndex] = action.payload;
      return newCars;
    case CarActions.DELETE:
      return state.filter(c => c.id !== action.payload);
    default:
      return state;
  }

}