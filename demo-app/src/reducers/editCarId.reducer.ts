import { Action } from 'redux';

import { CarActions, CarEditAction } from '../actions/car.actions';

export const editCarIdReducer = (state: number = -1, action: Action<string>) => {

  if (action.type === CarActions.EDIT) {
    return (action as CarEditAction).payload;
  }

  return -1;

}