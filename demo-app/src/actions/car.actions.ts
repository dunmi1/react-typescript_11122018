import { Action } from 'redux';

import { Car } from '../models/Car';

export enum CarActions {
  APPEND = '[Car] Append',
  REPLACE = '[Car] Replace',
  DELETE = '[Car] Delete',
  EDIT = '[Car] Edit',
  CANCEL = '[Car] Cancel',
}

export class CarAppendAction implements Action<string> {
  public readonly type = CarActions.APPEND;
  constructor(public payload: Car) { }
}

export class CarReplaceAction implements Action<string> {
  public readonly type = CarActions.REPLACE;
  constructor(public payload: Car) { }
}

export class CarDeleteAction implements Action<string> {
  public readonly type = CarActions.DELETE;
  constructor(public payload: number) { }
}

export class CarEditAction implements Action<string> {
  public readonly type = CarActions.EDIT;
  constructor(public payload: number) { }
}

export class CarCancelAction implements Action<string> {
  public readonly type = CarActions.CANCEL;
}

export type CarActionsUnion = CarAppendAction | CarReplaceAction |
  CarDeleteAction | CarEditAction | CarCancelAction;

export const appendCar = (car: Car) => new CarAppendAction(car);
export const replaceCar = (car: Car) => new CarReplaceAction(car);
export const deleteCar = (carId: number) => new CarDeleteAction(carId);
export const editCar = (carId: number) => new CarEditAction(carId);
export const cancelCar = () => new CarCancelAction();

export const actions = {
  appendCar, replaceCar, deleteCar, editCar, cancelCar,
};
