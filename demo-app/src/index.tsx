import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';


fetch('http://localhost:3050/cars', { method: 'POST' })
  // .then(res => res.json())
  // .then(cars => console.log(cars));

ajax.post('http://localhost:3050/cars')
  .pipe(map(ajaxResponse => ajaxResponse.response))
  // .subscribe(cars => console.log(cars));







// import * as React from 'react';
// import * as ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';

// import { carToolStore } from './carToolStore';
// import { CarToolContainer } from './containers/CarToolContainer';

// import './index.css';

// ReactDOM.render(
//   <Provider store={carToolStore}>
//     <CarToolContainer />
//   </Provider>,
//   document.querySelector('#root') as HTMLElement
// );

