import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { CarToolState } from '../models/CarToolState';
import { CarTool } from '../components/CarTool';
import { actions } from '../actions/car.actions';

const mapStateToProps = ({ cars, editCarId }: CarToolState) => ({ cars, editCarId });
const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
  onAppend: actions.appendCar,
  onReplace: actions.replaceCar,
  onDelete: actions.deleteCar,
  onEdit: actions.editCar,
  onCancel: actions.cancelCar,
}, dispatch);

export const CarToolContainer = connect(mapStateToProps, mapDispatchToProps)(CarTool);