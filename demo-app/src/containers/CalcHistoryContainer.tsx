import { connect } from 'react-redux';

import { CalcToolState } from '../models/CalcToolState';
import { UnorderedList } from 'src/components/UnorderedList';


export const CalcHistoryContainer = connect(
  ({ history }: CalcToolState) =>
    ({
      title: 'Calc History',
      items: history.map(historyItem => `op: ${historyItem.op} value: ${historyItem.value}`),
    }),
)(UnorderedList);