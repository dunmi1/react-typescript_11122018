import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CalcToolState } from '../models/CalcToolState';

import { AddAction, SubtractAction, MultiplyAction } from '../actions/calc.actions';
import { CalcTool } from '../components/CalcTool';

export const CalcToolContainer = connect(
  ({ result }: CalcToolState) => ({ result }), // mapStateToProps
  (dispatch) => bindActionCreators({
    onAdd: (num: number) => new AddAction(num),
    onSubtract: (num: number) => new SubtractAction(num),
    onMultiply: (num: number) => new MultiplyAction(num),
  }, dispatch), // mapDispatchToProps
)(CalcTool);
