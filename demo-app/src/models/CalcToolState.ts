import { CalcHistory } from './CalcHistory';
export interface CalcToolState {
  result: number;
  history: CalcHistory[];
};