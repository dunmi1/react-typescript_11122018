export interface Color {
  id?: number;
  color: string;
  hexCode: string;
}