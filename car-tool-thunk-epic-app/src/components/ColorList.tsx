import * as React from 'react';

import { Color } from '../models/Color';

export interface Props {
  colors: Color[];
}

export const ColorList = ({ colors }: Props) => {

  return <ul>
    {colors.map(color => <li key={color.id} style={{ color: color.hexCode }}>
      {color.color}
    </li>)}
  </ul>;

};