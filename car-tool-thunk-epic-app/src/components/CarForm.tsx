import * as React from 'react';

import { Car } from '../models/Car';
import { change } from '../utils/change';

export interface Props {
  buttonText: string;
  onSubmit: (car: Car) => void;
}

export interface State {
  make: string;
  model: string;
  year: number;
  color: string;
  price: number;
}

export class CarForm extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      make: '',
      model: '',
      year: 1900,
      color: '',
      price: 0,
    };
  }

  public submitCar = () => {

    this.props.onSubmit({
      ...this.state
    });

    this.setState({
      make: '',
      model: '',
      year: 1900,
      color: '',
      price: 0,
    });

  }

  public render() {
    return <form>
      <div>
        <label htmlFor="make-input">Make</label>
        <input type="text" name="make" id="make-input"
          value={this.state.make} onChange={change(this)} />
      </div>
      <div>
        <label htmlFor="model-input">Model</label>
        <input type="text" name="model" id="model-input"
          value={this.state.model} onChange={change(this)} />
      </div>
      <div>
        <label htmlFor="year-input">Year</label>
        <input type="number" name="year" id="year-input"
          value={this.state.year} onChange={change(this)} />
      </div>
      <div>
        <label htmlFor="color-input">Year</label>
        <input type="text" name="color" id="color-input"
          value={this.state.color} onChange={change(this)} />
      </div>
      <div>
        <label htmlFor="price-input">Price</label>
        <input type="number" name="price" id="price-input"
          value={this.state.price} onChange={change(this)} />
      </div>

      <button type="button" onClick={this.submitCar}>{this.props.buttonText}</button>

    </form>;
  }

}