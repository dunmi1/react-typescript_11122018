import { Car } from './models/Car'; 

export interface CarToolState {
  cars: Car[];
  editCarId: number;
  loading: boolean;
  errorMessage: string;
}