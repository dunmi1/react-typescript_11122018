import { AnyAction, Dispatch } from 'redux';

export const actionFromClass = () =>
  (next: Dispatch<AnyAction>) =>
    (action: AnyAction) => {
      next({ ...action });
    };