import * as React from 'react';

export type ChangeEventFormElement =
  React.ChangeEvent<HTMLInputElement> |
  React.ChangeEvent<HTMLSelectElement> |
  React.ChangeEvent<HTMLTextAreaElement>;

export const change = (component : React.Component) =>
  (e: ChangeEventFormElement) => {
    component.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value)
        : e.target.value,
    });
  };